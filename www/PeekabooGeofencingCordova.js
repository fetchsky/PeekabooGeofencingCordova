var exec = require("cordova/exec");

var PLUGIN_NAME = "PeekabooGeofencingCordova";

exports.startTracking = function(arg0, success, error) {
	exec(success, error, PLUGIN_NAME, "startTracking", [arg0]);
};

exports.setFirebaseIdentifier = function(arg0, success, error) {
	exec(success, error, PLUGIN_NAME, "setFirebaseIdentifier", [arg0]);
};

exports.onNotificationOpened = function(arg0, success, error) {
	exec(success, error, PLUGIN_NAME, "onNotificationOpened", [arg0]);
};

exports.onNotificationReceived = function(arg0, success, error) {
	exec(success, error, PLUGIN_NAME, "onNotificationReceived", [arg0]);
};
