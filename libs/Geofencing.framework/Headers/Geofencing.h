//
//  geofencing.h
//  geofencing
//
//  Created by Mac on 14/05/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for geofencing.
FOUNDATION_EXPORT double geofencingVersionNumber;

//! Project version string for geofencing.
FOUNDATION_EXPORT const unsigned char geofencingVersionString[];


@interface Geofencing : NSObject <UIApplicationDelegate>

@property (nonatomic) NSString* GEOFENCING_PARAMS_BASE_URL;
@property (nonatomic) NSString* GEOFENCING_PARAMS_INSTANCE_ID;

+(id)sharedManager;
-(void)trackLocation:(NSDictionary *)options;
-(void)setFirebaseInstanceId:(NSString *)instanceId;
-(void)onNotificationReceived:(NSDictionary *)notification;
-(void)onNotificationOpened:(NSDictionary *)notification;

@end
