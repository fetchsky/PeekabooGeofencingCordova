//
//  PeekabooGeofencing.m
//  myABL
//
//  Created by Mohammad Zain on 01/11/2019.
//

#import <Foundation/Foundation.h>
#import <Geofencing/Geofencing.h>
#import "PeekabooGeofencing.h"

@implementation PeekabooGeofencing {
    NSMutableDictionary* params;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}


-(void)initialize {
    if (params == nil) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [Geofencing.sharedManager trackLocation:params];
    });
}

- (void) startTracking:(NSDictionary *)args {
    if (params == nil) {
        params = [[NSMutableDictionary alloc] initWithDictionary:@{}];
    }
    [params addEntriesFromDictionary:args];
    [self initialize];
}

- (void) setInstaceId:(NSString *)instanceId {
    params[@"instanceId"] = instanceId;
    [Geofencing.sharedManager setFirebaseInstanceId:instanceId];
};

- (void) onAppStateChange:(NSDictionary *)options {
    [self initialize];
}

- (void) onNotificationReceived:(NSDictionary *)options {
    [Geofencing.sharedManager onNotificationReceived:options];
}

- (void) onNotificationOpened:(NSDictionary *)options {
    [Geofencing.sharedManager onNotificationOpened:options];
}

@end
