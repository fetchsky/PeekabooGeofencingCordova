//
//  AppDelegate+PeekabooGeofencingCordova.m
//
//  Created by Zain Sajjad on 12/09/19.
//
//
#import "AppDelegate+PeekabooGeofencingCordova.h"
#import <Geofencing/Geofencing.h>
#import "PeekabooGeofencing.h"

@implementation AppDelegate (PeekabooGeofencingCordova)

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [PeekabooGeofencing.sharedManager onAppStateChange:@{}];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [PeekabooGeofencing.sharedManager onAppStateChange:@{}];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [PeekabooGeofencing.sharedManager onAppStateChange:@{}];
}

@end
