/********* PeekabooGeofencingCordova.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <Geofencing/Geofencing.h>
#import "PeekabooGeofencing.h"

@interface PeekabooGeofencingCordova : CDVPlugin {
  // Member variables go here.
}

- (void)startTracking:(CDVInvokedUrlCommand*)command;
- (void)setFirebaseIdentifier:(CDVInvokedUrlCommand*)command;
- (void)onNotificationOpened:(CDVInvokedUrlCommand*)command;

@end

@implementation PeekabooGeofencingCordova {
}

- (void)startTracking:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSDictionary *args = [command.arguments objectAtIndex:0];
    if (args[@"baseUrl"] == nil || args[@"instanceId"] == nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"true"];
        [PeekabooGeofencing.sharedManager startTracking:args];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setFirebaseIdentifier:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* instanceId = [command.arguments objectAtIndex:0];
    if (!(instanceId != nil && [instanceId length] > 0)) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return;
    }
    [PeekabooGeofencing.sharedManager setFirebaseInstanceId:instanceId];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:instanceId];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void)onNotificationOpened:(CDVInvokedUrlCommand*)command
{
    [PeekabooGeofencing.sharedManager onNotificationOpened:[command.arguments objectAtIndex:0]];
}


- (void)onNotificationReceived:(CDVInvokedUrlCommand*)command
{
    [PeekabooGeofencing.sharedManager onNotificationReceived:[command.arguments objectAtIndex:0]];
}

@end
