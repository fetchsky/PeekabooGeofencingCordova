//
//  PeekabooGeofencing.h
//  myABL
//
//  Created by Mohammad Zain on 01/11/2019.
//

#ifndef PeekabooGeofencing_h
#define PeekabooGeofencing_h
@interface PeekabooGeofencing : NSObject

+(id)sharedManager;

- (void) startTracking:(NSDictionary *)args;
- (void) setInstaceId:(NSString *)instanceId;
- (void) onAppStateChange:(NSDictionary *)options;
- (void) onNotificationReceived:(NSDictionary *)options;

@end

#endif /* PeekabooGeofencing_h */
