package org.apache.cordova.peekaboo;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.firebase.iid.FirebaseInstanceId;
import com.peekaboo.geofencing.PeekabooGeofencingService;
import com.peekaboo.geofencing.PeekabooGeofencingParams;

import java.util.Iterator;

/**
 * This class echoes a string called from JavaScript.
 */
public class PeekabooGeofencingCordova extends CordovaPlugin {

    Context appContext;

    Bundle params = new Bundle();
    int REQUEST_CODE = 110;
    String[] PERMISSIONS = {
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_NETWORK_STATE",
            "android.permission.RECEIVE_BOOT_COMPLETED",
    };

    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults)
            throws JSONException {
        if (requestCode != REQUEST_CODE) {
            return;
        }
        cordova.getThreadPool().execute(() -> {
            try {
                int n = permissions.length;
                for (int i = 0; i < n; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        this.setFirebaseIdentifier();
                        return;
                    }
                    this.startTracking();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("startTracking")) {
            final JSONObject options = (args.length() > 0) ? args.getJSONObject(0) : new JSONObject();
            params = jsonToBundle(options);
            cordova.getThreadPool().execute(() -> {
                this.startTracking();
            });
            return true;
        }
        if (action.equals("setFirebaseIdentifier")) {
            final JSONObject options = (args.length() > 0) ? args.getJSONObject(0) : new JSONObject();
            params = jsonToBundle(options);
            cordova.getThreadPool().execute(() -> {
                this.setFirebaseIdentifier();
            });
            return true;
        }
        if (action.equals("onNotificationOpened")) {
            final JSONObject notification = (args.length() > 0) ? args.getJSONObject(0) : new JSONObject();
            cordova.getThreadPool().execute(() -> {
                try {
                    this.onNotificationOpened(notification);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
            return true;
        }
        return false;
    }

    private boolean hasPermissions() {
        for (int i = 0; i < PERMISSIONS.length; i++) {
            if (!cordova.hasPermission(PERMISSIONS[i])) {
                return false;
            }
        }
        return true;
    }

    private void requestPermissions() {
        try {
            cordova.requestPermissions(this, REQUEST_CODE, PERMISSIONS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bundle jsonToBundle(JSONObject jsonObject) throws JSONException {
        Bundle bundle = new Bundle();
        Iterator iter = jsonObject.keys();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            String value = jsonObject.getString(key);
            bundle.putString(key, value);
        }
        return bundle;
    }

    private void startTracking() {
        if (!hasPermissions()) {
            requestPermissions();
            return;
        }
        if (params.getString(PeekabooGeofencingParams.FIREBASE_INSTANCE_ID) == null) {
            params.putString(PeekabooGeofencingParams.FIREBASE_INSTANCE_ID, FirebaseInstanceId.getInstance().getToken());
        }
        params.putBoolean(PeekabooGeofencingParams.SHOW_LOCATION_LOCAL_NOTIFICATIONS, true);
        params.putString(PeekabooGeofencingParams.BASE_URL, params.getString("baseUrl"));
        appContext = cordova.getActivity().getApplicationContext();
        PeekabooGeofencingService.startTracking(appContext, params);
    }

    private void setFirebaseIdentifier() {
        if (params.getString(PeekabooGeofencingParams.FIREBASE_INSTANCE_ID) == null) {
            params.putString(PeekabooGeofencingParams.FIREBASE_INSTANCE_ID, FirebaseInstanceId.getInstance().getToken());
        }
        params.putBoolean(PeekabooGeofencingParams.SHOW_LOCATION_LOCAL_NOTIFICATIONS, true);
        params.putString(PeekabooGeofencingParams.BASE_URL, params.getString("baseUrl"));
        appContext = cordova.getActivity().getApplicationContext();
        PeekabooGeofencingService.setFirebaseInstanceId(appContext, params);
    }

    private void onNotificationOpened(JSONObject notification) throws JSONException {
        PeekabooGeofencingService.onNotificationOpened(jsonToBundle(notification));
    }
}
