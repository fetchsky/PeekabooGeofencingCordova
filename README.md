# Peekaboo Geofencing Cordova

## Installation Steps

- Install Plugin in your cordova project

```bash
cordova plugin add https://gitlab.com/fetchsky/PeekabooGeofencingCordova
```

### Android

To initialize Peekaboo Geofencing SDK use following method

```javascript
window.PeekabooGeofencingCordova.startTracking(YOUR_BASE_URL);
```

#### Note Peekaboo Geofencing Android uses FCM, please make sure you have integrated firebase in your app prior to this integration

### iOS

- Use this method when you want to initialize peekaboo connect

```javascript
var GEOFENCING_BASE_URL = "https://allied-geofencing-stage.peekaboo.guru";
function initializePeekaboo({ type, launchUrl }) {
	peekaboo.init({
		pkbc: "com.ofss.digx.mobile.android.allied",
		environment: "production", // stage, beta, production
		type: type || "deals", // deals, locator, stores, rewards
		country: "Pakistan",
		initialRoute: launchUrl || ""
	});
}

var isGeofencingInitialized = false;
function initializeGeofencing(fcmToken) {
	if (
		!fcmToken ||
		(fcmToken && fcmToken.indexOf("null") !== -1) ||
		isGeofencingInitialized
	) {
		return;
	}
	isGeofencingInitialized = true;
	PeekabooGeofencingCordova.setFirebaseIdentifier(fcmToken);
	PeekabooGeofencingCordova.startTracking(GEOFENCING_BASE_URL);
}

FCMPlugin.getToken(function(token) {
	initializeGeofencing(token);
});
FCMPlugin.onTokenRefresh(function(token) {
	initializeGeofencing(token);
});
FCMPlugin.onNotification(function(data) {
	if (data && data.wasTapped && data.source === "PEEKABOO") {
		PeekabooGeofencingCordova.onNotificationOpened(data);
		if ((data.launchUrl || "").startsWith("peekaboo://")) {
			initializePeekaboo({
				type: data.module,
				launchUrl: data.launchUrl
			});
		}
	}
	return;
});
```
